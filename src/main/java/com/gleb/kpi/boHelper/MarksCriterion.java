package com.gleb.kpi.boHelper;

/**
 * Created by gleb on 16.10.15.
 */
public class MarksCriterion {

    private int id;
    private String criterion;
    private int mark;

    public MarksCriterion(int id, String criterion, int mark) {
        this.id = id;
        this.criterion = criterion;
        this.mark = mark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCriterion() {
        return criterion;
    }

    public void setCriterion(String criterion) {
        this.criterion = criterion;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

}
