package com.gleb.kpi.rest;

import com.gleb.kpi.bo.BOSmart;
import com.gleb.kpi.service.ISmartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by gleb on 09.11.15.
 */
@Component
@Path("/smart")
public class SmartResource {

    @Autowired
    private ISmartService smartService;

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(BOSmart boSmart) {
        if (boSmart == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        smartService.saveSmart(boSmart);
        return Response.ok().build();
    }

    @GET
    @Path("/getUUID/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("uuid") String uuid) {
        if (uuid == null || uuid.equals("")) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        BOSmart boSmart = smartService.getSmartByUUID(uuid);
        return Response.ok().entity(boSmart).build();
    }

}
