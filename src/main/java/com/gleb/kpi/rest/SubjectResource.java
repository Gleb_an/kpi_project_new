package com.gleb.kpi.rest;

import com.gleb.kpi.bo.BOSubject;
import com.gleb.kpi.service.ISubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by gleb on 09.11.15.
 */
@Component
@Path("/subject")
public class SubjectResource {

    @Autowired
    private ISubjectService subjectService;

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(BOSubject boSubject) {
        if (boSubject == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        subjectService.saveSubject(boSubject);
        return Response.ok().build();
    }

    @GET
    @Path("/getUUID/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByUUID(@PathParam("uuid") String uuid) {
        if (uuid == null || uuid.equals("")) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        BOSubject boSubject = subjectService.getSubjectByUUID(uuid);
        if (boSubject == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().entity(boSubject).build();
    }

}
