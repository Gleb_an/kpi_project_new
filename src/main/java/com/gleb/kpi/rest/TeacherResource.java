package com.gleb.kpi.rest;

import com.gleb.kpi.bo.BOTeacher;
import com.gleb.kpi.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by gleb on 09.11.15.
 */
@Component
@Path("/teacher")
public class TeacherResource {

    @Autowired
    public ITeacherService teacherService;

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(BOTeacher boTeacher) {
        if (boTeacher == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        teacherService.saveTeacher(boTeacher);
        return Response.ok().build();
    }

    @GET
    @Path("/getUUID/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUUID(@PathParam("uuid") String uuid) {
        if (uuid == null || uuid.equals("")) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        BOTeacher boTeacher = teacherService.getTeacherByUUID(uuid);
        if (boTeacher == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().entity(boTeacher).build();
    }

}
