package com.gleb.kpi.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gleb.kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;

import com.gleb.kpi.bo.BOVoteMain;
import org.springframework.stereotype.Component;

/**
 * Created by gleb on 20.10.15.
 */
@Component
@Path("/vote")
public class VoteMainResource {

    @Autowired
    private IVoteMainService voteMainService;

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveVote(BOVoteMain boVoteMain) {
        if (boVoteMain == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        voteMainService.saveVote(boVoteMain);
        return Response.ok().build();
    }

    @GET
    @Path("/get/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVote(@PathParam("id") int id) {
        if (id < 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        BOVoteMain boVoteMain = voteMainService.getVote(id);
        if (boVoteMain == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().entity(boVoteMain).build();
    }

    @GET
    @Path("/get/{idT}/{idS}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByTeachAndSubj(@PathParam("idT") int idTeacher, @PathParam("idS") int idSubject) {
        if (idTeacher < 0 && idSubject < 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        BOVoteMain boVoteMain = voteMainService.getByTeachAndSubj(idTeacher, idSubject);
        if (boVoteMain == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().entity(boVoteMain).build();
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateVote(BOVoteMain boVoteMain) {
        if (boVoteMain == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        voteMainService.updateVote(boVoteMain);
        return Response.ok().build();
    }

}
