package com.gleb.kpi.rest;

import com.gleb.kpi.bo.BOMark;
import com.gleb.kpi.service.IMarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by gleb on 09.11.15.
 */
@Component
@Path("/mark")
public class MarkResource {

    @Autowired
    private IMarkService markService;

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(BOMark boMark) {
        if (boMark == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        markService.saveMark(boMark);
        return Response.ok().build();
    }

    @GET
    @Path("/get/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {
        if (id < 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        BOMark boMark = markService.getMark(id);
        if (boMark == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().entity(boMark).build();
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(BOMark boMark) {
        if (boMark == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        markService.updateMark(boMark);
        return Response.ok().build();
    }

}
