package com.gleb.kpi.mapping;

import com.gleb.kpi.bo.BOSubject;
import com.gleb.kpi.dao.object.DBSubject;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by gleb on 19.10.15.
 */
@Component
public class SubjectMapper {

    private Mapper mapper;

    @PostConstruct
    public void init() {
        mapper = new DozerBeanMapper();
    }

    public BOSubject map(DBSubject dbSubject) {
        if (dbSubject == null) {
            return null;
        }
//        BOSubject boSubject = new BOSubject();
//        boSubject.setId(dbSubject.getId());
//        boSubject.setUiid(dbSubject.getUiid());
        return mapper.map(dbSubject, BOSubject.class);
    }

    public DBSubject map(BOSubject boSubject) {
        if (boSubject == null) {
            return null;
        }
        return mapper.map(boSubject, DBSubject.class);
    }

}
