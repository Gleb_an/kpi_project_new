package com.gleb.kpi.mapping;

import com.gleb.kpi.bo.BOTeacher;
import com.gleb.kpi.dao.object.DBTeacher;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by gleb on 19.10.15.
 */
@Component
public class TeacherMapper {

    private Mapper mapper;

    @PostConstruct
    public void init() {
        mapper = new DozerBeanMapper();
    }

    public BOTeacher map(DBTeacher dbTeacher) {
        if (dbTeacher == null) {
            return null;
        }
//        BOTeacher boTeacher = new BOTeacher();
//        boTeacher.setId(dbTeacher.getId());
//        boTeacher.setUiid(dbTeacher.getUiid());
        return mapper.map(dbTeacher, BOTeacher.class);
    }

    public DBTeacher map(BOTeacher boTeacher) {
        if (boTeacher == null) {
            return null;
        }
        return mapper.map(boTeacher, DBTeacher.class);
    }

}
