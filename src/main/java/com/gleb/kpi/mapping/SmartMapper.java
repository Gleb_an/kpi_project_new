package com.gleb.kpi.mapping;

import com.gleb.kpi.bo.BOSmart;
import com.gleb.kpi.dao.object.DBSmart;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by gleb on 19.10.15.
 */
@Component
public class SmartMapper {

    private Mapper mapper;

    @PostConstruct
    public void init() {
        mapper = new DozerBeanMapper();
    }

    public BOSmart map(DBSmart dbSmart) {
        if (dbSmart == null) {
            return null;
        }
//        BOSmart boSmart = new BOSmart();
//        boSmart.setId(dbSmart.getId());
//        boSmart.setUiid(dbSmart.getUiid());
        return mapper.map(dbSmart, BOSmart.class);
    }

    public DBSmart map(BOSmart boSmart) {
        if (boSmart == null) {
            return null;
        }
        return mapper.map(boSmart, DBSmart.class);
    }

}
