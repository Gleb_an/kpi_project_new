package com.gleb.kpi.mapping;

import com.gleb.kpi.bo.BOMark;
import com.gleb.kpi.dao.object.DBMark;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by gleb on 19.10.15.
 */
@Component
public class MarkMapper {

    private Mapper mapper;

    @PostConstruct
    public void init() {
        mapper = new DozerBeanMapper();
    }

    public BOMark map(DBMark dbMark) {
        if (dbMark == null) {
            return null;
        }
//        BOMark boMark = new BOMark();
//        boMark.setId(dbMark.getId());
//        boMark.setCriterio1(dbMark.getCriterio1());
//        boMark.setCriterio2(dbMark.getCriterio2());
//        boMark.setCriterio3(dbMark.getCriterio3());
//        boMark.setCriterio4(dbMark.getCriterio4());
//        boMark.setCriterio5(dbMark.getCriterio5());
        return mapper.map(dbMark, BOMark.class);
    }

    public DBMark map(BOMark boMark) {
        if (boMark == null) {
            return null;
        }
        return mapper.map(boMark, DBMark.class);
    }

}
