package com.gleb.kpi.mapping;

import com.gleb.kpi.bo.BOVoteMain;
import com.gleb.kpi.dao.object.DBVoteMain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by gleb on 19.10.15.
 */
@Component
public class VoteMainMapper {

    @Autowired
    private SubjectMapper subjectMapper;

    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private SmartMapper smartMapper;

    @Autowired
    private MarkMapper markMapper;

    public BOVoteMain map(DBVoteMain dbVoteMain) {
        if (dbVoteMain == null) {
            return null;
        }
        BOVoteMain boVoteMain = new BOVoteMain();
        boVoteMain.setId(dbVoteMain.getId());
        boVoteMain.setBoSubject(subjectMapper.map(dbVoteMain.getDbSubject()));
        boVoteMain.setBoTeacher(teacherMapper.map(dbVoteMain.getDbTeacher()));
        boVoteMain.setBoSmart(smartMapper.map(dbVoteMain.getDbSmart()));
        boVoteMain.setBoMark(markMapper.map(dbVoteMain.getDbMark()));
        return boVoteMain;
    }

    public DBVoteMain map(BOVoteMain boVoteMain) {
        if (boVoteMain == null) {
            return null;
        }
        DBVoteMain dbVoteMain = new DBVoteMain();
        dbVoteMain.setId(boVoteMain.getId());
        dbVoteMain.setDbSubject(subjectMapper.map(boVoteMain.getBoSubject()));
        dbVoteMain.setDbTeacher(teacherMapper.map(boVoteMain.getBoTeacher()));
        dbVoteMain.setDbSmart(smartMapper.map(boVoteMain.getBoSmart()));
        dbVoteMain.setDbMark(markMapper.map(boVoteMain.getBoMark()));
        return dbVoteMain;
    }

}
