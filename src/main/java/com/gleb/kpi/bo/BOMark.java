package com.gleb.kpi.bo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by gleb on 16.10.15.
 */
@XmlRootElement
public class BOMark {

    private int id;
    private int criterio1;
    private int criterio2;
    private int criterio3;
    private int criterio4;
    private int criterio5;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCriterio1() {
        return criterio1;
    }

    public void setCriterio1(int criterio1) {
        this.criterio1 = criterio1;
    }

    public int getCriterio2() {
        return criterio2;
    }

    public void setCriterio2(int criterio2) {
        this.criterio2 = criterio2;
    }

    public int getCriterio3() {
        return criterio3;
    }

    public void setCriterio3(int criterio3) {
        this.criterio3 = criterio3;
    }

    public int getCriterio4() {
        return criterio4;
    }

    public void setCriterio4(int criterio4) {
        this.criterio4 = criterio4;
    }

    public int getCriterio5() {
        return criterio5;
    }

    public void setCriterio5(int criterio5) {
        this.criterio5 = criterio5;
    }

}
