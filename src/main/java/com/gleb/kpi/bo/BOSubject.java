package com.gleb.kpi.bo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by gleb on 16.10.15.
 */
@XmlRootElement
public class BOSubject {

    private int id;
    private String uiid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUiid() {
        return uiid;
    }

    public void setUiid(String uiid) {
        this.uiid = uiid;
    }

}
