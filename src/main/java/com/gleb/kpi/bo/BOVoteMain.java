package com.gleb.kpi.bo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by gleb on 19.10.15.
 */
@XmlRootElement
public class BOVoteMain {

    private int id;
    private BOTeacher boTeacher;
    private BOSubject boSubject;
    private BOSmart boSmart;
    private BOMark boMark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BOTeacher getBoTeacher() {
        return boTeacher;
    }

    public void setBoTeacher(BOTeacher boTeacher) {
        this.boTeacher = boTeacher;
    }

    public BOSubject getBoSubject() {
        return boSubject;
    }

    public void setBoSubject(BOSubject boSubject) {
        this.boSubject = boSubject;
    }

    public BOSmart getBoSmart() {
        return boSmart;
    }

    public void setBoSmart(BOSmart boSmart) {
        this.boSmart = boSmart;
    }

    public BOMark getBoMark() {
        return boMark;
    }

    public void setBoMark(BOMark boMark) {
        this.boMark = boMark;
    }
}
