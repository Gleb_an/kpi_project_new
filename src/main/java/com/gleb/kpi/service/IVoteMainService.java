package com.gleb.kpi.service;

import com.gleb.kpi.bo.BOVoteMain;

/**
 * Created by gleb on 19.10.15.
 */
public interface IVoteMainService {

    int saveVote(BOVoteMain boVoteMain);

    BOVoteMain getVote(int id);

    BOVoteMain getByTeachAndSubj(int idTeacher, int idSubject);

    boolean updateVote(BOVoteMain boVoteMain);

}
