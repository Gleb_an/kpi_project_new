package com.gleb.kpi.service;

import com.gleb.kpi.bo.BOTeacher;

/**
 * Created by gleb on 16.10.15.
 */
public interface ITeacherService {

    int saveTeacher(BOTeacher boTeacher);

    BOTeacher getTeacher(int id);

    BOTeacher getTeacherByUUID(String uuid);

    boolean updateTeacher(BOTeacher boTeacher);

}
