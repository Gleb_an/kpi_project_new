package com.gleb.kpi.service;

import com.gleb.kpi.bo.BOMark;

/**
 * Created by gleb on 16.10.15.
 */
public interface IMarkService {

    int saveMark(BOMark boMark);

    BOMark getMark(int id);

    boolean updateMark(BOMark boMark);

}
