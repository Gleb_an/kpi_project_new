package com.gleb.kpi.service.impl;

import com.gleb.kpi.bo.BOVoteMain;
import com.gleb.kpi.dao.IVoteMainDAO;
import com.gleb.kpi.dao.object.DBVoteMain;
import com.gleb.kpi.mapping.VoteMainMapper;
import com.gleb.kpi.service.IVoteMainService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gleb on 19.10.15.
 */
@Service
public class VoteMainServiceImpl implements IVoteMainService {

    @Autowired
    private IVoteMainDAO voteMainDAO;

    @Autowired
    private VoteMainMapper voteMainMapper;

    @Override
    public int saveVote(BOVoteMain boVoteMain) {
        DBVoteMain dbVoteMain = voteMainMapper.map(boVoteMain);
        return voteMainDAO.saveVote(dbVoteMain);
    }

    @Override
    public BOVoteMain getVote(int id) {
        DBVoteMain dbVoteMain = voteMainDAO.getVote(id);
        return voteMainMapper.map(dbVoteMain);
    }

    @Override
    public BOVoteMain getByTeachAndSubj(int idTeacher, int idSubject) {
        DBVoteMain dbVoteMain = voteMainDAO.getVoteByTeachAndSubj(idTeacher, idSubject);
        return voteMainMapper.map(dbVoteMain);
    }

    @Override
    public boolean updateVote(BOVoteMain boVoteMain) {
        DBVoteMain dbVoteMain = voteMainMapper.map(boVoteMain);
        return voteMainDAO.updateVote(dbVoteMain);
    }

}
