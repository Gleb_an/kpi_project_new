package com.gleb.kpi.service.impl;

import com.gleb.kpi.bo.BOSubject;
import com.gleb.kpi.dao.ISubjectDAO;
import com.gleb.kpi.dao.object.DBSubject;
import com.gleb.kpi.mapping.SubjectMapper;
import com.gleb.kpi.service.ISubjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gleb on 19.10.15.
 */
@Service
public class SubjectServiceImpl implements ISubjectService {

    @Autowired
    private ISubjectDAO subjectDAO;

    @Autowired
    private SubjectMapper subjectMapper;

    @Override
    public int saveSubject(BOSubject boSubject) {
        DBSubject dbSubject = subjectMapper.map(boSubject);
        return subjectDAO.saveSubject(dbSubject);
    }

    @Override
    public BOSubject getSubject(int id) {
        DBSubject dbSubject = subjectDAO.getSubject(id);
        return subjectMapper.map(dbSubject);
    }

    @Override
    public BOSubject getSubjectByUUID(String uuid) {
        DBSubject dbSubject = subjectDAO.getSubjectByUUID(uuid);
        return subjectMapper.map(dbSubject);
    }

    @Override
    public boolean updateSubject(BOSubject boSubject) {
        DBSubject dbSubject = subjectMapper.map(boSubject);
        return subjectDAO.updateSubject(dbSubject);
    }

}
