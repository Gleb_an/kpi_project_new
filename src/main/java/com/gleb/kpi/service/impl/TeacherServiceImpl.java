package com.gleb.kpi.service.impl;

import com.gleb.kpi.bo.BOTeacher;
import com.gleb.kpi.dao.ITeacherDAO;
import com.gleb.kpi.dao.object.DBTeacher;
import com.gleb.kpi.mapping.TeacherMapper;
import com.gleb.kpi.service.ITeacherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gleb on 19.10.15.
 */
@Service
public class TeacherServiceImpl implements ITeacherService {

    @Autowired
    private ITeacherDAO teacherDAO;

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public int saveTeacher(BOTeacher boTeacher) {
        DBTeacher dbTeacher = teacherMapper.map(boTeacher);
        return teacherDAO.saveTeacher(dbTeacher);
    }

    @Override
    public BOTeacher getTeacher(int id) {
        DBTeacher dbTeacher = teacherDAO.getTeacher(id);
        return teacherMapper.map(dbTeacher);
    }

    @Override
    public BOTeacher getTeacherByUUID(String uuid) {
        DBTeacher dbTeacher = teacherDAO.getTeacherByUUID(uuid);
        return teacherMapper.map(dbTeacher);
    }

    @Override
    public boolean updateTeacher(BOTeacher boTeacher) {
        DBTeacher dbTeacher = teacherMapper.map(boTeacher);
        return teacherDAO.updateTeacher(dbTeacher);
    }

}
