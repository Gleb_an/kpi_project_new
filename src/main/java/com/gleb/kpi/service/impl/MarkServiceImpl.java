package com.gleb.kpi.service.impl;

import com.gleb.kpi.bo.BOMark;
import com.gleb.kpi.dao.IMarkDAO;
import com.gleb.kpi.dao.object.DBMark;
import com.gleb.kpi.mapping.MarkMapper;
import com.gleb.kpi.service.IMarkService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gleb on 16.10.15.
 */
@Service
public class MarkServiceImpl implements IMarkService {

    @Autowired
    private MarkMapper markMapper;

    @Autowired
    private IMarkDAO markDAO;

    @Override
    public int saveMark(BOMark boMark) {
        DBMark dbMark = markMapper.map(boMark);
        return markDAO.saveMark(dbMark);
    }

    @Override
    public BOMark getMark(int id) {
        DBMark dbMark = markDAO.getMark(id);
        return markMapper.map(dbMark);
    }

    @Override
    public boolean updateMark(BOMark boMark) {
        DBMark dbMark = markMapper.map(boMark);
        return markDAO.updateMark(dbMark);
    }

}
