package com.gleb.kpi.service.impl;

import com.gleb.kpi.bo.BOSmart;
import com.gleb.kpi.dao.ISmartDAO;
import com.gleb.kpi.dao.object.DBSmart;
import com.gleb.kpi.mapping.SmartMapper;
import com.gleb.kpi.service.ISmartService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gleb on 19.10.15.
 */
@Service
public class SmartServiceImpl implements ISmartService {

    @Autowired
    private ISmartDAO smartDAO;

    @Autowired
    private SmartMapper smartMapper;

    @Override
    public int saveSmart(BOSmart boSmart) {
        DBSmart dbSmart = smartMapper.map(boSmart);
        return smartDAO.saveSmart(dbSmart);
    }

    @Override
    public BOSmart getSmart(int id) {
        DBSmart dbSmart = smartDAO.getSmart(id);
        return smartMapper.map(dbSmart);
    }

    @Override
    public BOSmart getSmartByUUID(String uuid) {
        DBSmart dbSmart = smartDAO.getSmartByUUID(uuid);
        return smartMapper.map(dbSmart);
    }

    @Override
    public boolean updateSmart(BOSmart boSmart) {
        DBSmart dbSmart = smartMapper.map(boSmart);
        return smartDAO.updateSmart(dbSmart);
    }

}
