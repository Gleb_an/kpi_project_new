package com.gleb.kpi.service;

import com.gleb.kpi.bo.BOSmart;

/**
 * Created by gleb on 16.10.15.
 */
public interface ISmartService {

    int saveSmart(BOSmart boSmart);

    BOSmart getSmart(int id);

    BOSmart getSmartByUUID(String uuid);

    boolean updateSmart(BOSmart boSmart);

}
