package com.gleb.kpi.service;

import com.gleb.kpi.bo.BOSubject;

/**
 * Created by gleb on 16.10.15.
 */
public interface ISubjectService {

    int saveSubject(BOSubject boSubject);

    BOSubject getSubject(int id);

    BOSubject getSubjectByUUID(String uuid);

    boolean updateSubject(BOSubject boSubject);

}
