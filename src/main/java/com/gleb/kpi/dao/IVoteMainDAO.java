package com.gleb.kpi.dao;

import com.gleb.kpi.dao.object.DBVoteMain;

/**
 * Created by gleb on 18.10.15.
 */
public interface IVoteMainDAO {

    int saveVote(DBVoteMain dbVote_main);

    DBVoteMain getVote(int id);

    DBVoteMain getVoteByTeachAndSubj(int idTeacher, int idSubject);

    boolean updateVote(DBVoteMain dbVote_main);

}
