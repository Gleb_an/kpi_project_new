package com.gleb.kpi.dao.object;

import javax.persistence.*;
import java.util.List;

/**
 * Created by gleb on 17.10.15.
 */
@Entity
@Table(name = DBMark.IDENTIFIER)
public class DBMark {
    public static final String IDENTIFIER = "mark";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mark")
    private int id;
    @Column(name = "criterio1")
    private int criterio1;
    @Column(name = "criterio2")
    private int criterio2;
    @Column(name = "criterio3")
    private int criterio3;
    @Column(name = "criterio4")
    private int criterio4;
    @Column(name = "criterio5")
    private int criterio5;
    @OneToMany(mappedBy = "dbMark", fetch = FetchType.LAZY)
    private List<DBVoteMain> dbVoteMains;

    public DBMark() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCriterio1() {
        return criterio1;
    }

    public void setCriterio1(int criterio1) {
        this.criterio1 = criterio1;
    }

    public int getCriterio2() {
        return criterio2;
    }

    public void setCriterio2(int criterio2) {
        this.criterio2 = criterio2;
    }

    public int getCriterio3() {
        return criterio3;
    }

    public void setCriterio3(int criterio3) {
        this.criterio3 = criterio3;
    }

    public int getCriterio4() {
        return criterio4;
    }

    public void setCriterio4(int criterio4) {
        this.criterio4 = criterio4;
    }

    public int getCriterio5() {
        return criterio5;
    }

    public void setCriterio5(int criterio5) {
        this.criterio5 = criterio5;
    }

    public List<DBVoteMain> getDbVoteMains() {
        return dbVoteMains;
    }

    public void setDbVoteMains(List<DBVoteMain> dbVoteMains) {
        this.dbVoteMains = dbVoteMains;
    }

}
