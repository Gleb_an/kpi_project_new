package com.gleb.kpi.dao.object;

import javax.persistence.*;
import java.util.List;

/**
 * Created by gleb on 16.10.15.
 */
@Entity
@Table(name = DBSmart.IDENTIFIER, uniqueConstraints = @UniqueConstraint(columnNames = {"uiid_smart"}))
public class DBSmart {
    public static final String IDENTIFIER = "smart";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_smart")
    private int id;
    @Column(name = "uiid_smart",unique = true)
    private String uiid;
    @OneToMany(mappedBy = "dbSmart", fetch = FetchType.LAZY)
    private List<DBVoteMain> dbVote_mains;

    public DBSmart() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUiid() {
        return uiid;
    }

    public void setUiid(String uiid) {
        this.uiid = uiid;
    }

    public List<DBVoteMain> getDbVote_mains() {
        return dbVote_mains;
    }

    public void setDbVote_mains(List<DBVoteMain> dbVote_mains) {
        this.dbVote_mains = dbVote_mains;
    }

}
