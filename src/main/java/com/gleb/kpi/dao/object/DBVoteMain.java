package com.gleb.kpi.dao.object;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by gleb on 16.10.15.
 */
@Entity
@Table(name = DBVoteMain.IDENTIFIER)
public class DBVoteMain implements Serializable {
    public static final String IDENTIFIER = "vote_main";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_main")
    private int id;
    @ManyToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id_teacher")
    private DBTeacher dbTeacher;
    @ManyToOne
    @JoinColumn(name = "subject_id", referencedColumnName = "id_subject")
    private DBSubject dbSubject;
    @ManyToOne
    @JoinColumn(name = "smart_id", referencedColumnName = "id_smart")
    private DBSmart dbSmart;
    @ManyToOne
    @JoinColumn(name = "mark_id", referencedColumnName = "id_mark")
    private DBMark dbMark;

    public DBVoteMain() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DBTeacher getDbTeacher() {
        return dbTeacher;
    }

    public void setDbTeacher(DBTeacher dbTeacher) {
        this.dbTeacher = dbTeacher;
    }

    public DBSubject getDbSubject() {
        return dbSubject;
    }

    public void setDbSubject(DBSubject dbSubject) {
        this.dbSubject = dbSubject;
    }

    public DBSmart getDbSmart() {
        return dbSmart;
    }

    public void setDbSmart(DBSmart dbSmart) {
        this.dbSmart = dbSmart;
    }

    public DBMark getDbMark() {
        return dbMark;
    }

    public void setDbMark(DBMark dbMark) {
        this.dbMark = dbMark;
    }

}
