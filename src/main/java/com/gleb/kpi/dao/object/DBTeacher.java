package com.gleb.kpi.dao.object;

import javax.persistence.*;
import java.util.List;

/**
 * Created by gleb on 16.10.15.
 */
@Entity
@Table(name = DBTeacher.IDENTIFIER, uniqueConstraints = @UniqueConstraint(columnNames = {"uiid_teacher"}))
public class DBTeacher {
    public static final String IDENTIFIER = "teacher";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_teacher")
    private int id;
    @Column(name = "uiid_teacher", unique = true)
    private String uiid;
    @OneToMany(mappedBy = "dbTeacher", fetch = FetchType.LAZY)
    private List<DBVoteMain> dbVote_mains;

    public DBTeacher() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUiid() {
        return uiid;
    }

    public void setUiid(String uiid) {
        this.uiid = uiid;
    }

    public List<DBVoteMain> getDbVote_mains() {
        return dbVote_mains;
    }

    public void setDbVote_mains(List<DBVoteMain> dbVote_mains) {
        this.dbVote_mains = dbVote_mains;
    }

}
