package com.gleb.kpi.dao;

import com.gleb.kpi.dao.object.DBSubject;

/**
 * Created by gleb on 16.10.15.
 */
public interface ISubjectDAO {

    int saveSubject(DBSubject dbSubject);

    DBSubject getSubject(int id);

    DBSubject getSubjectByUUID(String uuid);

    boolean updateSubject(DBSubject dbSubject);

}
