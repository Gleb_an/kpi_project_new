package com.gleb.kpi.dao;

import com.gleb.kpi.dao.object.DBTeacher;

/**
 * Created by gleb on 16.10.15.
 */
public interface ITeacherDAO {

    int saveTeacher(DBTeacher dbTeacher);

    DBTeacher getTeacher(int id);

    DBTeacher getTeacherByUUID(String uuid);

    boolean updateTeacher(DBTeacher dbTeacher);

}
