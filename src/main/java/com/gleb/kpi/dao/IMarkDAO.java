package com.gleb.kpi.dao;

import com.gleb.kpi.dao.object.DBMark;

/**
 * Created by gleb on 16.10.15.
 */
public interface IMarkDAO {

    int saveMark(DBMark dbMark);

    DBMark getMark(int id);

    boolean updateMark(DBMark dbMark);

}
