package com.gleb.kpi.dao;

import com.gleb.kpi.dao.object.DBSmart;

/**
 * Created by gleb on 16.10.15.
 */
public interface ISmartDAO {

    int saveSmart(DBSmart dbSmart);

    DBSmart getSmart(int id);

    DBSmart getSmartByUUID(String uuid);

    boolean updateSmart(DBSmart dbSmart);

}
