package com.gleb.kpi.dao.impl;

import com.gleb.kpi.dao.IMarkDAO;
import com.gleb.kpi.dao.object.DBMark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

/**
 * Created by gleb on 16.10.15.
 */
@Repository
@Transactional
public class MarkDAOImpl implements IMarkDAO {

    @Autowired
    private EntityManagerFactory factory;

    @Override
    public int saveMark(DBMark dbMark) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(dbMark);
        transaction.commit();
        return dbMark.getId();
    }

    @Override
    public DBMark getMark(int id) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        DBMark dbMark = entityManager.find(DBMark.class, id);
        transaction.commit();
        return dbMark;
    }

    @Override
    public boolean updateMark(DBMark dbMark) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(dbMark);
        transaction.commit();
        return true;
    }

}
