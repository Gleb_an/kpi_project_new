package com.gleb.kpi.dao.impl;

import com.gleb.kpi.dao.ISubjectDAO;
import com.gleb.kpi.dao.object.DBSubject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

/**
 * Created by gleb on 16.10.15.
 */
@Repository
@Transactional
public class SubjectDAOImpl implements ISubjectDAO {

    @Autowired
    private EntityManagerFactory factory;

    @Override
    public int saveSubject(DBSubject dbSubject) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(dbSubject);
        transaction.commit();
        return dbSubject.getId();
    }

    @Override
    public DBSubject getSubject(int id) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        DBSubject dbSubject = entityManager.find(DBSubject.class, id);
        transaction.commit();
        return dbSubject;
    }

    @Override
    public DBSubject getSubjectByUUID(String uuid) {
        EntityManager entityManager = factory.createEntityManager();
        String request = String.format("FROM %s WHERE uiid_subject = \'%s\'", DBSubject.class.getName(), uuid);
        DBSubject dbSubject = (DBSubject) entityManager.createQuery(request, DBSubject.class).getSingleResult();
        return dbSubject;
    }

    @Override
    public boolean updateSubject(DBSubject dbSubject) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(dbSubject);
        transaction.commit();
        return true;
    }

}
