package com.gleb.kpi.dao.impl;

import com.gleb.kpi.dao.ISmartDAO;
import com.gleb.kpi.dao.object.DBSmart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

/**
 * Created by gleb on 16.10.15.
 */
@Repository
@Transactional
public class SmartDAOImpl implements ISmartDAO {

    @Autowired
    private EntityManagerFactory factory;

    @Override
    public int saveSmart(DBSmart dbSmart) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(dbSmart);
        transaction.commit();
        return dbSmart.getId();
    }

    @Override
    public DBSmart getSmart(int id) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        DBSmart dbSmart = entityManager.find(DBSmart.class, id);
        transaction.commit();
        return dbSmart;
    }

    @Override
    public DBSmart getSmartByUUID(String uuid) {
        EntityManager entityManager = factory.createEntityManager();
        String request = String.format("FROM %s WHERE uiid_smart = \'%s\'", DBSmart.class.getName(), uuid);
        DBSmart dbSmart = (DBSmart) entityManager.createQuery(request, DBSmart.class).getSingleResult();
        return dbSmart;
    }

    @Override
    public boolean updateSmart(DBSmart dbSmart) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(dbSmart);
        transaction.commit();
        return true;
    }

}
