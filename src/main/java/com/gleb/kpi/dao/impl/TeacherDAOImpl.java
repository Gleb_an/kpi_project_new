package com.gleb.kpi.dao.impl;

import com.gleb.kpi.dao.ITeacherDAO;
import com.gleb.kpi.dao.object.DBTeacher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 * Created by gleb on 16.10.15.
 */
@Repository
@Transactional
public class TeacherDAOImpl implements ITeacherDAO {

    @Autowired
    private EntityManagerFactory factory;

    @Transactional
    @Override
    public int saveTeacher(DBTeacher dbTeacher) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(dbTeacher);
        transaction.commit();
        return dbTeacher.getId();
    }

    @Override
    public DBTeacher getTeacher(int id) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        DBTeacher dbTeacher = entityManager.find(DBTeacher.class, id);
        transaction.commit();
        return dbTeacher;
    }

    @Override
    public DBTeacher getTeacherByUUID(String uuid) {
        EntityManager entityManager = factory.createEntityManager();
        String request = String.format("FROM %s WHERE uiid_teacher = \'%s\'", DBTeacher.class.getName(), uuid);
        Query query = entityManager.createQuery(request);
        DBTeacher dbTeacher = (DBTeacher) query.getSingleResult();
        return dbTeacher;
    }

    @Override
    public boolean updateTeacher(DBTeacher dbTeacher) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(dbTeacher);
        transaction.commit();
        return true;
    }

}
