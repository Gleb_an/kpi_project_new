package com.gleb.kpi.dao.impl;

import com.gleb.kpi.dao.IVoteMainDAO;
import com.gleb.kpi.dao.object.DBVoteMain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

/**
 * Created by gleb on 18.10.15.
 */
@Repository
@Transactional
public class VoteMainDAOImpl implements IVoteMainDAO {

    @Autowired
    private EntityManagerFactory factory;

    @Transactional
    @Override
    public int saveVote(DBVoteMain dbVote_main) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(dbVote_main);
        transaction.commit();
        return dbVote_main.getId();
    }

    @Override
    public DBVoteMain getVote(int id) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        DBVoteMain dbVote_main = entityManager.find(DBVoteMain.class, id);
        transaction.commit();
        return dbVote_main;
    }

    @Override
    public DBVoteMain getVoteByTeachAndSubj(int idTeacher, int idSubject) {
        EntityManager entityManager = factory.createEntityManager();
        String request = String.format("FROM %s WHERE teacher_id = %d AND subject_id = %d", DBVoteMain.class.getName(), idTeacher, idSubject);
        DBVoteMain dbVoteMain = (DBVoteMain) entityManager.createQuery(request, DBVoteMain.class).getSingleResult();
        return dbVoteMain;
    }

    @Override
    public boolean updateVote(DBVoteMain dbVote_main) {
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(dbVote_main);
        transaction.commit();
        return true;
    }

}
