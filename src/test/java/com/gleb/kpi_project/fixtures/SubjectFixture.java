package com.gleb.kpi_project.fixtures;

import com.gleb.kpi.dao.object.DBSubject;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Created by gleb on 07.11.15.
 */
public class SubjectFixture {

    public static void cherSubject(DBSubject dbSubject, DBSubject getSubject) {
        Assert.assertThat(dbSubject.getId(), equalTo(getSubject.getId()));
        Assert.assertThat(dbSubject.getUiid(), equalTo(getSubject.getUiid()));
    }

}
