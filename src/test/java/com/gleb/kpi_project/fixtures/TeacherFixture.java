package com.gleb.kpi_project.fixtures;

import com.gleb.kpi.dao.object.DBTeacher;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Created by gleb on 07.11.15.
 */
public class TeacherFixture {

    public static void cherTeacher(DBTeacher dbTeacher, DBTeacher getTeacher) {
        Assert.assertThat(dbTeacher.getId(), equalTo(getTeacher.getId()));
        Assert.assertThat(dbTeacher.getUiid(), equalTo(getTeacher.getUiid()));
    }

}
