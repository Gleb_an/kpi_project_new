package com.gleb.kpi_project.fixtures;

import com.gleb.kpi.dao.object.DBVoteMain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by gleb on 07.11.15.
 */
public class VoteMainFixture {

    public static void checkVote(DBVoteMain dbVoteMain, DBVoteMain dbVoteMain1) {
        assertThat(dbVoteMain.getId(), equalTo(dbVoteMain1.getId()));
        MarkFixture.checkDBMark(dbVoteMain.getDbMark(), dbVoteMain1.getDbMark());
        SmartFixture.cherSmart(dbVoteMain.getDbSmart(), dbVoteMain1.getDbSmart());
        SubjectFixture.cherSubject(dbVoteMain.getDbSubject(), dbVoteMain1.getDbSubject());
        TeacherFixture.cherTeacher(dbVoteMain.getDbTeacher(), dbVoteMain1.getDbTeacher());
    }

}
