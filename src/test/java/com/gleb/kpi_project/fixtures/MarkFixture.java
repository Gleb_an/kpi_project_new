package com.gleb.kpi_project.fixtures;

import com.gleb.kpi.dao.object.DBMark;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by gleb on 07.11.15.
 */
public class MarkFixture {

    public static void checkDBMark(DBMark dbMark, DBMark dbMark1) {
        assertThat(dbMark.getId(), equalTo(dbMark1.getId()));
        assertThat(dbMark.getCriterio1(), equalTo(dbMark1.getCriterio1()));
        assertThat(dbMark.getCriterio2(), equalTo(dbMark1.getCriterio2()));
        assertThat(dbMark.getCriterio3(), equalTo(dbMark1.getCriterio3()));
        assertThat(dbMark.getCriterio4(), equalTo(dbMark1.getCriterio4()));
        assertThat(dbMark.getCriterio5(), equalTo(dbMark1.getCriterio5()));
    }

}
