package com.gleb.kpi_project.fixtures;

import com.gleb.kpi.dao.object.DBSmart;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Created by gleb on 07.11.15.
 */
public class SmartFixture {

    public static void cherSmart(DBSmart dbSmart, DBSmart getSmart) {
        Assert.assertThat(dbSmart.getId(), equalTo(getSmart.getId()));
        Assert.assertThat(dbSmart.getUiid(), equalTo(getSmart.getUiid()));
    }

}
