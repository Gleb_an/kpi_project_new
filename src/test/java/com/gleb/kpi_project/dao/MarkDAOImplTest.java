package com.gleb.kpi_project.dao;

import static com.gleb.kpi_project.fixtures.MarkFixture.checkDBMark;

import com.gleb.kpi.dao.IMarkDAO;
import com.gleb.kpi.dao.object.DBMark;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Created by gleb on 18.10.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApp_context.xml"})
public class MarkDAOImplTest {

    @Autowired
    private IMarkDAO markDAO;

    @Autowired
    private EntityManagerFactory factory;

    @Before
    public void clear() {
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE " + DBMark.IDENTIFIER, DBMark.class).executeUpdate();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Test
    public void markTest() {
        DBMark dbMark = new DBMark();
        //dbMark.setId(1);
        dbMark.setCriterio1(1);
        dbMark.setCriterio2(1);
        dbMark.setCriterio3(1);
        dbMark.setCriterio4(1);
        dbMark.setCriterio5(1);
        markDAO.saveMark(dbMark);
        
        DBMark getDBMark = markDAO.getMark(dbMark.getId());
        
        checkDBMark(dbMark, getDBMark);

        dbMark.setCriterio1(2);
        dbMark.setCriterio2(2);
        dbMark.setCriterio3(2);
        dbMark.setCriterio4(2);
        dbMark.setCriterio5(2);
        if (markDAO.updateMark(dbMark)) {
            getDBMark = markDAO.getMark(dbMark.getId());
        }
        checkDBMark(dbMark, getDBMark);
    }

}
