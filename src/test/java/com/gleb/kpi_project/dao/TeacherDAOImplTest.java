package com.gleb.kpi_project.dao;

import static com.gleb.kpi_project.fixtures.TeacherFixture.cherTeacher;

import com.gleb.kpi.dao.ITeacherDAO;
import com.gleb.kpi.dao.object.DBMark;
import com.gleb.kpi.dao.object.DBTeacher;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import java.util.UUID;

/**
 * Created by gleb on 18.10.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApp_context.xml"})
public class TeacherDAOImplTest {

    @Autowired
    private ITeacherDAO teacherDAO;

    @Autowired
    private EntityManagerFactory factory;

    @Before
    public void clear() {
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE " + DBTeacher.IDENTIFIER, DBMark.class).executeUpdate();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Test
    public void test() {
        DBTeacher dbTeacher = new DBTeacher();
        //dbTeacher.setId(1);
        dbTeacher.setUiid(UUID.randomUUID().toString());
        teacherDAO.saveTeacher(dbTeacher);

        DBTeacher dbTeacher1 = new DBTeacher();
        dbTeacher1.setUiid(UUID.randomUUID().toString());
        teacherDAO.saveTeacher(dbTeacher1);

        dbTeacher = new DBTeacher();
        dbTeacher.setUiid(UUID.randomUUID().toString());
        teacherDAO.saveTeacher(dbTeacher);

        DBTeacher getTeacher = teacherDAO.getTeacher(dbTeacher.getId());
        cherTeacher(dbTeacher, getTeacher);

        dbTeacher.setUiid(UUID.randomUUID().toString());
        if (teacherDAO.updateTeacher(dbTeacher)) {
            getTeacher = teacherDAO.getTeacher(dbTeacher.getId());
        }

        cherTeacher(dbTeacher, getTeacher);

        cherTeacher(dbTeacher, teacherDAO.getTeacherByUUID(dbTeacher.getUiid()));

    }

}
