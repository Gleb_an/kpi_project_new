package com.gleb.kpi_project.dao;

import static com.gleb.kpi_project.fixtures.SmartFixture.cherSmart;

import com.gleb.kpi.dao.ISmartDAO;
import com.gleb.kpi.dao.object.DBMark;
import com.gleb.kpi.dao.object.DBSmart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Created by gleb on 18.10.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApp_context.xml"})
public class SmartDAOImplTest {

    @Autowired
    private ISmartDAO smartDAO;

    @Autowired
    private EntityManagerFactory factory;

    @Before
    public void clear() {
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE " + DBSmart.IDENTIFIER, DBMark.class).executeUpdate();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
        entityManager.getTransaction().commit();
    }


    @Test
    public void test() {
        DBSmart dbSmart = new DBSmart();
        //dbSmart.setId(1);
        dbSmart.setUiid(UUID.randomUUID().toString());
        smartDAO.saveSmart(dbSmart);

        DBSmart getSmart = smartDAO.getSmart(dbSmart.getId());
        cherSmart(dbSmart, getSmart);

        dbSmart.setUiid(UUID.randomUUID().toString());
        if (smartDAO.updateSmart(dbSmart)) {
            getSmart = smartDAO.getSmart(dbSmart.getId());
        }

        cherSmart(dbSmart, getSmart);

        cherSmart(dbSmart, smartDAO.getSmartByUUID(dbSmart.getUiid()));

    }

}
