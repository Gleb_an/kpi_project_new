package com.gleb.kpi_project.dao;

import static com.gleb.kpi_project.fixtures.VoteMainFixture.checkVote;

import com.gleb.kpi.dao.*;
import com.gleb.kpi.dao.object.DBMark;
import com.gleb.kpi.dao.object.DBVoteMain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Created by gleb on 18.10.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApp_context.xml"})
public class VoteMainDAOImplTest {

    @Autowired
    private IMarkDAO iMarkDAO;

    @Autowired
    private ISmartDAO smartDAO;

    @Autowired
    private ISubjectDAO subjectDAO;

    @Autowired
    private ITeacherDAO teacherDAO;

    @Autowired
    private IVoteMainDAO iVoteMainDAO;

    @Autowired
    private EntityManagerFactory factory;

    @Before
    public void clear() {
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE " + DBVoteMain.IDENTIFIER, DBMark.class).executeUpdate();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Test
    public void test() {
        DBVoteMain dbVoteMain = new DBVoteMain();
        //dbVoteMain.setId(1);
        dbVoteMain.setDbMark(iMarkDAO.getMark(1));
        dbVoteMain.setDbSmart(smartDAO.getSmart(1));
        dbVoteMain.setDbSubject(subjectDAO.getSubject(1));
        dbVoteMain.setDbTeacher(teacherDAO.getTeacher(1));

        iVoteMainDAO.saveVote(dbVoteMain);

        DBVoteMain dbVoteMain1 = iVoteMainDAO.getVoteByTeachAndSubj(1, 1);

        checkVote(dbVoteMain, dbVoteMain1);

    }

}
