package com.gleb.kpi_project.dao;

import static com.gleb.kpi_project.fixtures.SubjectFixture.cherSubject;

import com.gleb.kpi.dao.ISubjectDAO;
import com.gleb.kpi.dao.object.DBSubject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import java.util.UUID;

/**
 * Created by gleb on 18.10.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({ @ContextConfiguration("classpath:testApp_context.xml") })
public class SubjectDAOImplTest {

    @Autowired
    private ISubjectDAO subjectDAO;

    @Autowired
    private EntityManagerFactory factory;

    @Before
    public void clear() {
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE " + DBSubject.IDENTIFIER, DBSubject.class).executeUpdate();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Test
    public void test() {
        DBSubject dbSubject = new DBSubject();
        //dbSubject.setId(1);
        dbSubject.setUiid(UUID.randomUUID().toString());
        subjectDAO.saveSubject(dbSubject);

        DBSubject getSubject = subjectDAO.getSubject(dbSubject.getId());
        cherSubject(dbSubject, getSubject);

        dbSubject.setUiid(UUID.randomUUID().toString());
        if (subjectDAO.updateSubject(dbSubject)) {
            getSubject = subjectDAO.getSubject(dbSubject.getId());
        }

        cherSubject(dbSubject, getSubject);

        cherSubject(dbSubject, subjectDAO.getSubjectByUUID(dbSubject.getUiid()));

    }

}
